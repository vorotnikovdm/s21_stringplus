#include "s21_test_string.h"

START_TEST(sscanf_char_test) {
  char a = ' ', a1 = ' ', a2 = ' ', a3 = ' ';
  char b = ' ', b1 = ' ', b2 = ' ', b3 = ' ';
  int v1 = sscanf("Text 1 2 3 4 5 6", "%c%c%*c%c", &a, &a1, &a3);
  int v2 = s21_sscanf("Text 1 2 3 4 5 6", "%c%c%*c%c", &b, &b1, &b3);
  ck_assert_int_eq(a, b);
  ck_assert_int_eq(a1, b1);
  ck_assert_int_eq(a2, b2);
  ck_assert_int_eq(a3, b3);
  ck_assert_int_eq(v1, v2);

  v1 = sscanf("Text 1 2 3 4 5 6", "%c %c %c %c", &a, &a1, &a2, &a3);
  v2 = s21_sscanf("Text 1 2 3 4 5 6", "%c %c %c %c", &b, &b1, &b2, &b3);
  ck_assert_int_eq(a, b);
  ck_assert_int_eq(a1, b1);
  ck_assert_int_eq(a2, b2);
  ck_assert_int_eq(a3, b3);
  ck_assert_int_eq(v1, v2);

  v1 = sscanf("1 2 3 4 5 6", "%c%c%c%c", &a, &a1, &a2, &a3);
  v2 = s21_sscanf("1 2 3 4 5 6", "%c%c%c%c", &b, &b1, &b2, &b3);
  ck_assert_int_eq(a, b);
  ck_assert_int_eq(a1, b1);
  ck_assert_int_eq(a2, b2);
  ck_assert_int_eq(a3, b3);
  ck_assert_int_eq(v1, v2);

  v1 = sscanf("1   11", "%c%c %c %c", &a, &a1, &a2, &a3);
  v2 = s21_sscanf("1   11", "%c%c %c %c", &b, &b1, &b2, &b3);
  ck_assert_int_eq(a, b);
  ck_assert_int_eq(a1, b1);
  ck_assert_int_eq(a2, b2);
  ck_assert_int_eq(a3, b3);
  ck_assert_int_eq(v1, v2);

  v1 = sscanf("text = 1,2,3,45679", "text = %c,%c,%c,%c", &a, &a1, &a2, &a3);
  v2 = s21_sscanf("text = 1,text2,3,45678", "text = %c,text%c,%c,%c", &b, &b1,
                  &b2, &b3);
  ck_assert_int_eq(a, b);
  ck_assert_int_eq(a1, b1);
  ck_assert_int_eq(a2, b2);
  ck_assert_int_eq(a3, b3);
  ck_assert_int_eq(v1, v2);

  v1 = sscanf("text = 1,text2,       3,45678", "text = %*c,text%c, %c,%c", &a1,
              &a2, &a3);
  v2 = s21_sscanf("text = 1,text2,       3,45678", "text = %*c,text%c, %c,%c",
                  &b1, &b2, &b3);
  ck_assert_int_eq(a, b);
  ck_assert_int_eq(a1, b1);
  ck_assert_int_eq(a2, b2);
  ck_assert_int_eq(a3, b3);
  ck_assert_int_eq(v1, v2);

  v1 = sscanf("123456789", "%c,%c%c%c", &a, &a1, &a2, &a3);
  v2 = s21_sscanf("123456789", "%c,%c%c%c", &b, &b1, &b2, &b3);

  ck_assert_int_eq(a, b);
  ck_assert_int_eq(a1, b1);
  ck_assert_int_eq(a2, b2);
  ck_assert_int_eq(a3, b3);
  ck_assert_int_eq(v1, v2);
}
END_TEST

Suite *s21_sscanf_char_test() {
  Suite *s = NULL;
  TCase *tc_core = NULL;

  s = suite_create("s21_sscanf_char");
  tc_core = tcase_create("Core");
  tcase_add_test(tc_core, sscanf_char_test);
  suite_add_tcase(s, tc_core);
  return s;
}