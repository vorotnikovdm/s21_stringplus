#include "s21_string.h"

void *s21_memchr(const void *str, int c, s21_size_t n) {
  unsigned char *check = (unsigned char *)str;
  while (n-- > 0) {
    if (*check == c) {
      return check;
    }
    check++;
  }
  return s21_NULL;
}