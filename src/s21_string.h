#ifndef C2_S21_STRINGPLUS_S21_STRINGPLUS_H_
#define C2_S21_STRINGPLUS_S21_STRINGPLUS_H_

#include <limits.h>
#include <math.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#define s21_size_t unsigned long long
#define s21_NULL (void *)0

#define TRUE 1
#define FALSE 0

// char *specificators = "cdieEfgGosuxXpn%";

typedef struct {
  int widht;
  int suppression;
  int length_modifiers;
  int space;
} spec_sscanf;

void *s21_memchr(const void *, int, s21_size_t);
int s21_memcmp(const void *, const void *, s21_size_t);
void *s21_memcpy(void *, const void *, s21_size_t);
void *s21_memset(void *, int, s21_size_t);
char *s21_strncat(char *, const char *, s21_size_t);
char *s21_strcat(char *dest, const char *src);
char *s21_strchr(const char *, int);
char *s21_strrchr(const char *str, int c);
int s21_strncmp(const char *, const char *, s21_size_t);
int s21_strcmp(const char *str1, const char *str2);
char *s21_strncpy(char *, const char *, s21_size_t);
char *s21_strcpy(char *dest, const char *src);
s21_size_t s21_strcspn(const char *, const char *);
char *s21_strerror(int);
s21_size_t s21_strlen(const char *);
char *s21_strpbrk(const char *, const char *);
char *s21_strstr(const char *, const char *);
char *s21_strtok(char *, const char *);
void *s21_to_upper(const char *str);
void *s21_to_lower(const char *str);
void *s21_insert(const char *src, const char *str, s21_size_t start_index);
void *s21_trim(const char *src, const char *trim_chars);
int s21_sprintf(char *str, const char *format, ...);
//функции относящееся sscanf

int s21_sscanf(const char *str, const char *format, ...);
const char *format_modifiers(const char *format, spec_sscanf *spec_ss);
const char *format_lenght(const char *format, spec_sscanf *spec_ss,
                          char *widht_char, char *specifier);
const char *specificator_logic(spec_sscanf *spec_ss, char *specifier,
                               const char *copy_str, va_list *ap,
                               int *error_flag, int *n_step_str);
int determine_number_type(const char *str);
unsigned long int extract_number(const char *str, int count, int *error_flag);
long long int convert_octal(const char *str, int count, int *error_flag);
long long int convert_hexadecimal(const char *str, int count, int *error_flag);
long double convert_to_float(const char *d_str, int count, int *error_flag,
                             int count_widht, spec_sscanf *spec_ss);

//функции спецификаторы
const char *specifer_c(va_list *ap, const char *copy_str, spec_sscanf *spec_ss);
const char *specifer_s(va_list *ap, const char *copy_str, spec_sscanf *spec_ss);
const char *specifer_d(va_list *ap, const char *copy_str, spec_sscanf *spec_ss,
                       int *error_flag);
const char *specifer_u(va_list *ap, const char *copy_str, spec_sscanf *spec_ss,
                       int *error_flag);
const char *specifer_percent(const char *copy_str, int *error_flag);
const char *specifer_p(va_list *ap, const char *copy_str, spec_sscanf *spec_ss);
const char *specifer_o(va_list *ap, const char *copy_str, spec_sscanf *spec_ss,
                       int *error_flag);
const char *specifer_x(va_list *ap, const char *copy_str, spec_sscanf *spec_ss,
                       int *error_flag);
const char *specifer_o_negative(va_list *ap, const char *copy_str,
                                spec_sscanf *spec_ss, int *error_flag);
const char *specifer_x_negative(va_list *ap, const char *copy_str,
                                spec_sscanf *spec_ss, int *error_flag);
const char *specifer_float(va_list *ap, const char *copy_str,
                           spec_sscanf *spec_ss, int *error_flag);
void specifer_system_selection(va_list *ap, const char *copy_str,
                               spec_sscanf *spec_ss, int *error_flag);

#endif  // C2_S21_STRINGPLUS_S21_STRINGPLUS_H_
