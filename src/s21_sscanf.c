#include "s21_string.h"

int s21_sscanf(const char *str, const char *format, ...) {
  va_list ap;
  va_start(ap, format);
  int error_str = 0, count_success = 0;
  const char *copy_str = str;
  int n_step_str = 0;
  while (*format && error_str != 1) {
    if (*format == '%') {
      spec_sscanf spec_ss = {0};
      char widht_char = ' ', specifier = ' ';
      const char *cop = copy_str;
      format = format_modifiers(++format, &spec_ss);
      format = format_lenght(format, &spec_ss, &widht_char, &specifier);
      copy_str = specificator_logic(&spec_ss, &specifier, copy_str, &ap,
                                    &error_str, &n_step_str);
      n_step_str += copy_str - cop;
      if (error_str != 1 && spec_ss.suppression != 1 && specifier != '%' &&
          specifier != 'n') {
        count_success++;
      }
      format++;
    } else if (*format != '%' && *format != ' ') {
      if (*copy_str != *format) {
        error_str = 1;
        break;
      }
      format++;
      n_step_str++;
      copy_str++;
    } else if (*format == ' ') {
      format++;
      while (*copy_str && *copy_str == ' ') {
        copy_str++;
        n_step_str++;
      }
    }
  }
  va_end(ap);
  return count_success;
}

const char *specificator_logic(spec_sscanf *spec_ss, char *specifier,
                               const char *copy_str, va_list *ap,
                               int *error_flag, int *n_step_str) {
  if (*specifier == 'c') {
    copy_str = specifer_c(ap, copy_str, spec_ss);
  } else if (*specifier == 's') {
    copy_str = specifer_s(ap, copy_str, spec_ss);
  } else if (*specifier == 'd') {
    copy_str = specifer_d(ap, copy_str, spec_ss, error_flag);
  } else if (*specifier == 'u') {
    copy_str = specifer_u(ap, copy_str, spec_ss, error_flag);
  } else if (*specifier == 'n') {
    int *s = va_arg(*ap, int *);
    *s = *n_step_str;
  } else if (*specifier == '%') {
    copy_str = specifer_percent(copy_str, error_flag);
  } else if (*specifier == 'p') {
    copy_str = specifer_p(ap, copy_str, spec_ss);
  } else if (*specifier == 'o') {
    copy_str = specifer_o(ap, copy_str, spec_ss, error_flag);
  } else if (*specifier == 'x' || *specifier == 'X') {
    copy_str = specifer_x(ap, copy_str, spec_ss, error_flag);
  } else if (*specifier == 'i') {
    specifer_system_selection(ap, copy_str, spec_ss, error_flag);
  } else if (*specifier == 'e' || *specifier == 'E' || *specifier == 'f' ||
             *specifier == 'g' || *specifier == 'G') {
    copy_str = specifer_float(ap, copy_str, spec_ss, error_flag);
  }

  return copy_str;
}