#include "s21_string.h"

char *s21_strcat(char *dest, const char *src) {
  int dest_len = s21_strlen(dest), src_len = s21_strlen(src);
  for (int i = 0; i < src_len && src[i] != '\0'; i++) {
    dest[dest_len + i] = src[i];
  }
  dest[dest_len + src_len] = '\0';
  return dest;
}
