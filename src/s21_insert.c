#include "s21_string.h"

void *s21_insert(const char *src, const char *str, s21_size_t start_index) {
  char *result = s21_NULL;
  if (src && str) {
    s21_size_t src_len = s21_strlen(src), str_len = s21_strlen(str);
    if (start_index <= (src_len + str_len)) {
      result = malloc((src_len + str_len + 1) * sizeof(char));
      if (result) {
        s21_strncpy(result, src, start_index);
        result[start_index] = '\0';
        s21_strcat(result, str);
        s21_strcat(result, src + start_index);
      }
    }
  }
  return (void *)result;
}
