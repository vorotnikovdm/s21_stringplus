#include "s21_string.h"

char *s21_strcpy(char *dest, const char *src) {
  int src_len = s21_strlen(src);
  for (int i = 0; i < src_len && src[i] != '\0'; i++) {
    dest[i] = src[i];
  }
  dest[src_len] = '\0';
  return dest;
}
