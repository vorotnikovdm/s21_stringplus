#include "s21_string.h"

void *s21_to_upper(const char *str) {
  char *result = s21_NULL;
  if (str) {
    result = malloc((s21_strlen(str) + 1) * sizeof(char));
  }
  if (result) {
    s21_strcpy(result, str);
    for (s21_size_t i = 0; i < s21_strlen(str) && result[i] != '\0'; i++) {
      if (result[i] >= 'a' && result[i] <= 'z') {
        result[i] = result[i] - 32;
      }
    }
    result[s21_strlen(str)] = '\0';
  }
  return (void *)result;
}
