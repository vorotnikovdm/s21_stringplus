#include "s21_string.h"

void *s21_trim(const char *src, const char *trim_chars) {
  char *result = s21_NULL;
  int flag = 0;
  if (src) {
    result = calloc((s21_strlen(src) + 1), sizeof(char));
    if (result) {
      if (trim_chars != s21_NULL && s21_strlen(trim_chars) != 0) {
        for (s21_size_t i = 0, res_i = 0; i < s21_strlen(src); i++) {
          flag = 0;
          for (s21_size_t j = 0; j < s21_strlen(trim_chars); j++) {
            if (src[i] == trim_chars[j]) {
              flag = 1;
            }
          }
          if (flag == 0) {
            result[res_i] = src[i];
            res_i++;
          }
        }
      } else {
        s21_strcpy(result, src);
      }
    }
  }
  return (void *)result;
}
