#include "s21_string.h"

const char *format_modifiers(const char *format, spec_sscanf *spec_ss) {
  int first_zero = 0;
  if (*format == '*') {
    spec_ss->suppression = 1;
    format++;
  } else if (('1' <= *format && *format <= '9') ||
             ('0' <= *format && *format <= '9' && first_zero == 1)) {
    while (*format) {
      if (('1' <= *format && *format <= '9') ||
          ('0' <= *format && *format <= '9' && first_zero == 1)) {
        spec_ss->widht *= 10;
        spec_ss->widht += *format - '0';
        first_zero = 1;
        format++;
      } else {
        break;
      }
    }
  }
  return format;
}

const char *format_lenght(const char *format, spec_sscanf *spec_ss,
                          char *widht_char, char *specifier) {
  if (*format == 'h') {
    spec_ss->length_modifiers = 1;
    *widht_char = *format;
    format++;
    *specifier = *format;
  } else if (*format == 'l') {
    spec_ss->length_modifiers = 2;
    *widht_char = *format;
    format++;
    *specifier = *format;
  } else if (*format == 'L') {
    spec_ss->length_modifiers = 3;
    *widht_char = *format;
    format++;
    *specifier = *format;
  } else {
    *specifier = *format;
  }
  return format;
}

int determine_number_type(const char *str) {
  int len = s21_strlen(str);  //!!!!!!!!!!!!!!!!!!
  int i = 0;
  int base = 10;

  if (str[0] == '0' && len > 1) {
    if (str[1] == 'x' || str[1] == 'X') {
      base = 16;
      i = 2;
    } else {
      base = 8;
      i = 1;
    }
  } else if (str[0] == '-' && len > 2) {
    if (str[1] == '0') {
      if (str[2] == 'x' || str[2] == 'X') {
        base = 16;
        i = 2;
      } else {
        base = 8;
        i = 1;
      }
    }
  }

  while (i < len) {
    char c = str[i];
    if (c >= '0' && c <= '9') {
      if (c - '0' >= base) {
        return -1;
      }
    } else if (base == 16 &&
               ((c >= 'a' && c <= 'f') || (c >= 'A' && c <= 'F'))) {
    } else if (i == 0 && c == '-') {
    } else {
      return -1;
    }
    i++;
  }

  return base;
}

unsigned long int extract_number(const char *str, int count, int *error_flag) {
  unsigned long int num = 0;
  int flag_start = 1;
  int minus_flag = 0;
  while (count > 0) {
    if ('0' <= *str && *str <= '9') {
      num *= 10;
      num += *str - '0';
      flag_start = 0;
    } else if ('-' == *str && minus_flag == 0) {
      minus_flag = 1;
    } else if ('0' != *str) {
      if (num == 0) {
        *error_flag = 1;
      }
      break;
    } else if (*str > 56 && flag_start == 1) {
      *error_flag = 1;
      break;
    } else if (*str > 56 && flag_start == 0) {
      break;
    }
    count--;
    str++;
  }
  if (minus_flag == 1) {
    num *= -1;
  }
  return num;
}

long long int convert_octal(const char *str, int count, int *error_flag) {
  int minus_flag = 0;
  int flag_start = 1;
  long long int num = 0;

  while (count > 0) {
    if ('0' <= *str && *str <= '7') {
      num += (*str - '0') * pow(8, count - 1);
      flag_start = 0;
    } else if ('-' == *str && minus_flag == 0) {
      minus_flag = 1;
    } else if ('0' != *str) {
      *error_flag = 1;
      break;
    } else if (*str > 55 && flag_start == 1) {
      *error_flag = 1;
      break;
    } else if (*str > 55 && flag_start == 0) {
      break;
    }
    count--;
    str++;
  }

  if (minus_flag == 1) {
    num *= -1;
  }

  return num;
}

long long int convert_hexadecimal(const char *str, int count, int *error_flag) {
  int minus_flag = 0;
  int flag_x = 1;
  long long int num = 0;

  while (count > 0) {
    if ('x' == *str && flag_x == 0) {
      *error_flag = 1;
      break;
    } else if ('1' <= *str && *str <= '9') {
      num += (*str - '0') * pow(16, count - 1);
      flag_x = 0;
    } else if ((*str > 64 && *str < 71) || (*str > 96 && *str < 103)) {
      switch (*str) {
        case 'a':
        case 'A':
          num += 10 * pow(16, count - 1);
          break;
        case 'b':
        case 'B':
          num += 11 * pow(16, count - 1);
          break;
        case 'c':
        case 'C':
          num += 12 * pow(16, count - 1);
          break;
        case 'd':
        case 'D':
          num += 13 * pow(16, count - 1);
          break;
        case 'e':
        case 'E':
          num += 14 * pow(16, count - 1);
          break;
        case 'f':
        case 'F':
          num += 15 * pow(16, count - 1);
          break;
        default:
          *error_flag = 1;
          break;
      }
      flag_x = 0;
    } else if ('-' == *str && minus_flag == 0) {
      minus_flag = 1;
    } else if ('0' != *str && ('x' != *str && 'X' != *str)) {
      *error_flag = 1;
      break;
    }

    count--;
    str++;
  }

  if (minus_flag == 1) {
    num *= -1;
  }
  return num;
}

long double convert_to_float(const char *d_str, int count, int *error_flag,
                             int count_widht, spec_sscanf *spec_ss) {
  long double num = 0.0;
  char result_str[s21_strlen(d_str) + 1];                //Заменить
  s21_memcpy(result_str, d_str, s21_strlen(d_str) + 1);  //заменить!!!!!!!!!
  if (spec_ss->widht != 0) {
    s21_memset(result_str, 0, s21_strlen(d_str) + 1);  //Заменить!!!!!!!!!!!
    s21_memcpy(result_str, d_str, count_widht);        //Заменить!!!
  }
  if (count == 0) {
    *error_flag = 1;
  } else {
    num = atof(result_str);
  }

  return num;
}