#include "s21_string.h"

const char *specifer_c(va_list *ap, const char *copy_str,
                       spec_sscanf *spec_ss) {
  if (spec_ss->suppression != 1) {
    char *s = va_arg(*ap, char *);
    *s = '\0';
    *s = *copy_str;
  }
  return ++copy_str;
}

const char *specifer_s(va_list *ap, const char *copy_str,
                       spec_sscanf *spec_ss) {
  while (*copy_str == ' ' && *copy_str != '\0') {
    copy_str++;
  }
  const char *c_str = copy_str;
  int count_widht = 0;
  while (*copy_str != ' ' && *copy_str != '\0' &&
         (spec_ss->widht == 0 || spec_ss->widht > count_widht)) {
    copy_str++;
    count_widht++;
  }
  int count = copy_str - c_str;
  if (spec_ss->suppression != 1) {
    char *s = va_arg(*ap, char *);
    s21_memcpy(s, c_str, count);  //Заменить на нормальную реализацию!!!!
    s[count] = '\0';
  }
  return copy_str;
}

const char *specifer_d(va_list *ap, const char *copy_str, spec_sscanf *spec_ss,
                       int *error_flag) {
  while (*copy_str == ' ' && *copy_str != '\0') {
    copy_str++;
  }

  int count_widht = 0;
  const char *d_str = copy_str;
  while (*copy_str != ' ' && *copy_str != '\0' && *copy_str <= 57 &&
         (spec_ss->widht == 0 || spec_ss->widht > count_widht)) {
    copy_str++;
    count_widht++;
  }
  int count = copy_str - d_str;

  if (count == 0) {
    *error_flag = 1;
  }

  if (spec_ss->suppression != 1) {
    if (spec_ss->length_modifiers == 1) {
      short int *s = va_arg(*ap, short int *);
      short int res = extract_number(d_str, count, error_flag);
      if (*error_flag != 1) {
        *s = res;
      }
    } else if (spec_ss->length_modifiers == 2) {
      long *s = va_arg(*ap, long *);
      long int res = extract_number(d_str, count, error_flag);
      if (*error_flag != 1) {
        *s = res;
      }
    } else {
      int *s = va_arg(*ap, int *);
      int res = extract_number(d_str, count, error_flag);
      if (*error_flag != 1) {
        *s = res;
      }
    }
  }
  return copy_str;
}

const char *specifer_u(va_list *ap, const char *copy_str, spec_sscanf *spec_ss,
                       int *error_flag) {
  while (*copy_str == ' ' && *copy_str != '\0') {
    copy_str++;
  }

  int count_widht = 0;
  const char *d_str = copy_str;
  while (*copy_str != ' ' && *copy_str != '\0' && *copy_str <= 57 &&
         (spec_ss->widht == 0 || spec_ss->widht > count_widht)) {
    copy_str++;
    count_widht++;
  }
  int count = copy_str - d_str;

  if (count == 0) {
    *error_flag = 1;
  }

  if (spec_ss->suppression != 1) {
    if (spec_ss->length_modifiers == 1) {
      unsigned short int *s = va_arg(*ap, unsigned short int *);
      unsigned short int res = extract_number(d_str, count, error_flag);
      if (*error_flag != 1) {
        *s = res;
      }
    } else if (spec_ss->length_modifiers == 2) {
      unsigned long int *s = va_arg(*ap, unsigned long int *);
      unsigned long int res = extract_number(d_str, count, error_flag);
      if (*error_flag != 1) {
        *s = res;
      }
    } else {
      unsigned int *s = va_arg(*ap, unsigned int *);
      unsigned int res = extract_number(d_str, count, error_flag);
      if (*error_flag != 1) {
        *s = res;
      }
    }
  }
  return copy_str;
}

const char *specifer_percent(const char *copy_str, int *error_flag) {
  if (*copy_str == ' ') {
    copy_str++;
  }

  if (*copy_str != '%') {
    *error_flag = 1;
  }

  return ++copy_str;
}

const char *specifer_p(va_list *ap, const char *copy_str,
                       spec_sscanf *spec_ss) {
  while (*copy_str == ' ' && *copy_str != '\0') {
    copy_str++;
  }
  const char *c_str = copy_str;
  int count_widht = 0;
  while (*copy_str != ' ' && *copy_str != '\0' &&
         (spec_ss->widht == 0 || spec_ss->widht > count_widht)) {
    copy_str++;
    count_widht++;
  }
  if (spec_ss->suppression != 1) {
    long int **s = va_arg(*ap, long int **);
    int count = copy_str - c_str;
    char text_s[100];
    s21_memcpy(text_s, c_str, count);
    text_s[count] = '\0';
    *s = (long int *)strtol(text_s, NULL, 16);
  }
  return copy_str;
}

const char *specifer_o(va_list *ap, const char *copy_str, spec_sscanf *spec_ss,
                       int *error_flag) {
  while (*copy_str == ' ' && *copy_str != '\0') {
    copy_str++;
  }

  int count_widht = 0;
  const char *d_str = copy_str;
  while (*copy_str != ' ' && *copy_str != '\0' && *copy_str <= 57 &&
         *copy_str != '8' && *copy_str != '9' &&
         (spec_ss->widht == 0 || spec_ss->widht > count_widht)) {
    copy_str++;
    count_widht++;
  }

  int count = copy_str - d_str;

  if (count == 0) {
    *error_flag = 1;
  }

  if (spec_ss->suppression != 1) {
    if (spec_ss->length_modifiers == 1) {
      unsigned short int *s = va_arg(*ap, unsigned short int *);
      unsigned short int res = convert_octal(d_str, count, error_flag);
      if (*error_flag != 1) {
        *s = res;
      }
    } else if (spec_ss->length_modifiers == 2) {
      unsigned long int *s = va_arg(*ap, unsigned long int *);
      unsigned long int res = convert_octal(d_str, count, error_flag);
      if (*error_flag != 1) {
        *s = res;
      }
    } else {
      unsigned int *s = va_arg(*ap, unsigned int *);
      unsigned int res = convert_octal(d_str, count, error_flag);
      if (*error_flag != 1) {
        *s = res;
      }
    }
  }
  return copy_str;
}

const char *specifer_x(va_list *ap, const char *copy_str, spec_sscanf *spec_ss,
                       int *error_flag) {
  while (*copy_str == ' ' && *copy_str != '\0') {
    copy_str++;
  }

  int count_widht = 0;
  const char *d_str = copy_str;
  while (*copy_str != ' ' && *copy_str != '\0' &&
         ((*copy_str > 47 && *copy_str < 58) ||
          (*copy_str > 64 && *copy_str < 71) ||
          (*copy_str > 96 && *copy_str < 103) ||
          (*copy_str == 88 || *copy_str == 120)) &&
         (spec_ss->widht == 0 || spec_ss->widht > count_widht)) {
    copy_str++;
    count_widht++;
  }

  int count = copy_str - d_str;
  if (count == 0) {
    *error_flag = 1;
  }

  if (spec_ss->suppression != 1) {
    if (spec_ss->length_modifiers == 1) {
      unsigned short int *s = va_arg(*ap, unsigned short int *);
      unsigned short int res = convert_hexadecimal(d_str, count, error_flag);
      if (*error_flag != 1) {
        *s = res;
      }
    } else if (spec_ss->length_modifiers == 2) {
      unsigned long int *s = va_arg(*ap, unsigned long int *);
      unsigned long int res = convert_hexadecimal(d_str, count, error_flag);
      if (*error_flag != 1) {
        *s = res;
      }
    } else {
      unsigned int *s = va_arg(*ap, unsigned int *);
      unsigned int res = convert_hexadecimal(d_str, count, error_flag);
      if (*error_flag != 1) {
        *s = res;
      }
    }
  }
  return copy_str;
}

const char *specifer_o_negative(va_list *ap, const char *copy_str,
                                spec_sscanf *spec_ss, int *error_flag) {
  while (*copy_str == ' ' && *copy_str != '\0') {
    copy_str++;
  }

  int count_widht = 0;
  const char *d_str = copy_str;
  while (*copy_str != ' ' && *copy_str != '\0' && *copy_str <= 57 &&
         *copy_str != '8' && *copy_str != '9' &&
         (spec_ss->widht == 0 || spec_ss->widht > count_widht)) {
    copy_str++;
    count_widht++;
  }

  int count = copy_str - d_str;

  if (count == 0) {
    *error_flag = 1;
  }

  if (spec_ss->suppression != 1) {
    if (spec_ss->length_modifiers == 1) {
      short int *s = va_arg(*ap, short int *);
      short int res = convert_octal(d_str, count, error_flag);
      if (*error_flag != 1) {
        *s = res;
      }
    } else if (spec_ss->length_modifiers == 2) {
      long int *s = va_arg(*ap, long int *);
      long int res = convert_octal(d_str, count, error_flag);
      if (*error_flag != 1) {
        *s = res;
      }
    } else {
      int *s = va_arg(*ap, int *);
      int res = convert_octal(d_str, count, error_flag);
      if (*error_flag != 1) {
        *s = res;
      }
    }
  }
  return copy_str;
}

const char *specifer_x_negative(va_list *ap, const char *copy_str,
                                spec_sscanf *spec_ss, int *error_flag) {
  while (*copy_str == ' ' && *copy_str != '\0') {
    copy_str++;
  }

  int count_widht = 0;
  const char *d_str = copy_str;
  while (*copy_str != ' ' && *copy_str != '\0' &&
         ((*copy_str > 47 && *copy_str < 58) ||
          (*copy_str > 64 && *copy_str < 71) ||
          (*copy_str > 96 && *copy_str < 103) ||
          (*copy_str == 88 || *copy_str == 120)) &&
         (spec_ss->widht == 0 || spec_ss->widht > count_widht)) {
    copy_str++;
    count_widht++;
  }

  int count = copy_str - d_str;
  if (count == 0) {
    *error_flag = 1;
  }

  if (spec_ss->suppression != 1) {
    if (spec_ss->length_modifiers == 1) {
      short int *s = va_arg(*ap, short int *);
      short int res = convert_hexadecimal(d_str, count, error_flag);
      if (*error_flag != 1) {
        *s = res;
      }
    } else if (spec_ss->length_modifiers == 2) {
      long int *s = va_arg(*ap, long int *);
      long int res = convert_hexadecimal(d_str, count, error_flag);
      if (*error_flag != 1) {
        *s = res;
      }
    } else {
      int *s = va_arg(*ap, int *);
      int res = convert_hexadecimal(d_str, count, error_flag);
      if (*error_flag != 1) {
        *s = res;
      }
    }
  }
  return copy_str;
}

const char *specifer_float(va_list *ap, const char *copy_str,
                           spec_sscanf *spec_ss, int *error_flag) {
  while (*copy_str == ' ' && *copy_str != '\0') {
    copy_str++;
  }

  int count_widht = 0;
  const char *d_str = copy_str;
  while (*copy_str != ' ' && *copy_str != '\0' &&
         ((*copy_str >= '0' && *copy_str <= '9') ||
          (*copy_str == '.' || *copy_str == 'e' || *copy_str == 'E' ||
           *copy_str == '-' || *copy_str == '+')) &&
         (spec_ss->widht == 0 || spec_ss->widht > count_widht)) {
    copy_str++;
    count_widht++;
  }

  int count = copy_str - d_str;

  if (count == 0) {
    *error_flag = 1;
  }

  if (spec_ss->suppression != 1) {
    if (spec_ss->length_modifiers == 3) {
      long double *s = va_arg(*ap, long double *);
      long double res =
          convert_to_float(d_str, count, error_flag, count_widht, spec_ss);
      if (*error_flag != 1) {
        *s = res;
      }
    } else {
      float *s = va_arg(*ap, float *);
      float res =
          convert_to_float(d_str, count, error_flag, count_widht, spec_ss);
      if (*error_flag != 1) {
        *s = res;
      }
    }
  }
  return copy_str;
}

void specifer_system_selection(va_list *ap, const char *copy_str,
                               spec_sscanf *spec_ss, int *error_flag) {
  while (*copy_str == ' ' && *copy_str != '\0') {
    copy_str++;
  }
  int type = determine_number_type(copy_str);
  if (type == 10) {
    copy_str = specifer_d(ap, copy_str, spec_ss, error_flag);
  } else if (type == 8) {
    copy_str = specifer_o_negative(ap, copy_str, spec_ss, error_flag);
  } else if (type == 16) {
    copy_str = specifer_x_negative(ap, copy_str, spec_ss, error_flag);
  } else if (type == -1) {
    *error_flag = 1;
  }
}