#include "s21_string.h"

char *s21_strstr(const char *haystack, const char *needle) {
  s21_size_t len = s21_strlen(needle);
  char *res = s21_NULL;
  if (len == 0) {
    res = (char *)haystack;
  }
  while (*haystack) {
    if (*haystack == *needle && s21_strncmp(haystack, needle, len) == 0) {
      res = (char *)haystack;
      break;
    }
    haystack++;
  }
  return res;
}