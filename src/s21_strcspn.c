#include "s21_string.h"

s21_size_t s21_strcspn(const char *str1, const char *str2) {
  s21_size_t res = 0;
  while (*str1 != '\0' && *str1 != *str2) {
    ++res;
    ++str1;
  }

  return res;
}